(function (angular) {
  'use strict';

  angular.module('angularMaterialFormBuilder')
    .directive('checkboxItem', CheckboxItem);

  function CheckboxItem() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/checkbox-item/checkbox-item.html',
      scope: {
        item: '='
      },
      controller: CheckboxItemCtrl,
      controllerAs: 'Checkbox',
      bindToController: true
    };

    return directive;
  }

  /*@ngInject*/
  function CheckboxItemCtrl(Utils, $element) {
    this.Element = $element;

    Utils.extend(this.item, {
      config: {
        maxSelections: 1,
        direction:'horizontal'
      },
      options: [{
        value: '',
        selected: false
      }]
    });
  }

  CheckboxItemCtrl.prototype.deleteOption = function (index) {
    this.item.options.splice(index, 1);
  };

  CheckboxItemCtrl.prototype.addOption = function () {
    this.item.options.push({
      value: '',
      selected: false
    });

    setTimeout(function() {
      var options = this.Element.find('input');
      var addedOption = options[options.length - 1];
      addedOption.focus();
    }.bind(this), 0);
  };

})(angular);
