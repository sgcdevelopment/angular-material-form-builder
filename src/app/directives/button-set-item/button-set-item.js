(function (angular) {
  'use strict';

  angular.module('angularMaterialFormBuilder')
    .directive('buttonSetItem', ButtonSet);

  function ButtonSet() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/button-set-item/button-set-item.html',
      scope: {
        item: '='
      },
      controller: ButtonSetCtrl,
      controllerAs: 'ButtonSet',
      bindToController: true
    };

    return directive;
  }

  /*@ngInject*/
  function ButtonSetCtrl(Utils, $element) {
    this.Element = $element;
    Utils.extend(this.item, {
      config: {},
      options: [{
        value: '',
        id: + new Date(),
        clicked:false
      }]
    });
  }

  ButtonSetCtrl.prototype.deleteOption = function (index) {
    this.item.options.splice(index, 1);
  };

  ButtonSetCtrl.prototype.addOption = function () {
    this.item.options.push({
      value: '',
      id: + new Date(),
      clicked:false
    });

    setTimeout(function() {
      var options = this.Element.find('input');
      var addedOption = options[options.length - 1];
      addedOption.focus();
    }.bind(this), 0);
  };

})(angular);
