(function (angular) {
  'use strict';

  angular.module('angularMaterialFormBuilder')
    .directive('buttonSetView', ButtonSetView);

  /*@ngInject*/
  function ButtonSetView($timeout) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/button-set-item/button-set-view.html',
      scope: {
        formItem: '=',
        isDisabled: '=',
        isPreview: '&',
        form: '='
      },
      controller: ButtonSetViewCtrl,
      controllerAs: 'ButtonSetView',
      bindToController: true,
      link: linker
    };

    function linker(scope, elem, attrs, ctrl) {

      //this timeout is placed here in order to make sure that the creator directive of this view is finished its work
      $timeout(function() {
        ctrl.init();
      }, 50);
    }

    return directive;
  }

  /*@ngInject*/
  function ButtonSetViewCtrl(Utils) {
    this.Utils = Utils;
  }

  ButtonSetViewCtrl.prototype.init = function () {

    this.Utils.extend(this.formItem, {
      config: {},
      options: []
    });
  };


})(angular);
