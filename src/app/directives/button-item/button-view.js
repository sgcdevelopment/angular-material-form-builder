(function (angular) {
  'use strict';

  angular.module('angularMaterialFormBuilder')
    .directive('buttonView', ButtonView);

  /*@ngInject*/
  function ButtonView($timeout) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/button-item/button-view.html',
      scope: {
        formItem: '=',
        form: '=',
        isDisabled: '=',
      },
      controller: ButtonViewCtrl,
      controllerAs: 'ButtonView',
      bindToController: true,
      link: linker
    };

    function linker(scope, elem, attrs, ctrl) {

      //this timeout is placed here in order to make sure that the creator directive of this view is finished its work
      $timeout(function() {
        ctrl.init();
      }, 50);
    }

    return directive;
  }

  /*@ngInject*/
  function ButtonViewCtrl(Utils) {
    this.Utils = Utils;
  }

  ButtonViewCtrl.prototype.init = function () {

    this.Utils.extend(this.formItem, {
      config: {},
      clicked:false
    });

  };


})(angular);
