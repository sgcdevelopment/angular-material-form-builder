(function (angular) {
  'use strict';

  angular.module('angularMaterialFormBuilder')
    .directive('buttonItem', ButtonItem);

  function ButtonItem() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/button-item/button-item.html',
      scope: {
        item: '='
      },
      controller: ButtonItem,
      controllerAs: 'Button',
      bindToController: true
    };

    return directive;
  }

  /*@ngInject*/
  function ButtonItemCtrl(Utils, $element) {
    this.Element = $element;

    Utils.extend(this.item, {
      config: {
        type: 'text'
      }
    });
  }

})(angular);
